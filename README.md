# sitecore-xc-smartdisplays

The Future of Voice Commerce with Smart Displays Powered by Sitecore Experience Commerce. Next-generation shopping experience with the Amazon Echo Show, powered by Sitecore Experience Commerce.

## Update the following default values in the index.js file

- **hhcDemoHostUrl** = 'sitecore-commerce-domain.com' replace with the domain hosting your Sitecore Commerce website

- **awsLambdaHostUrl** = 'lambda-host-domain.amazonaws.com' replace with the Alexa hosting domain

- **authKey** = 'ENTER AUTH KEY' replace with your API auth key

- **background_image_url** = 'https://s3bucket-domain/images/store-front-background.png' replace with background image url

- **const logo_image_url** = 'https://s3bucket-domain/images/logo.png' replace with logo url

## Update the following values in the skill.json file to point to your logos

- "smallIconUri": "https://s3bucket-domain/images/habitat-small-logo.png",
- "largeIconUri": "https://s3bucket-domain/images/habitat-large-logo.png"

## Update the following values in the welcome.json file to point to your cart logos

- "headerAttributionImage": "https://s3bucket-domain/images/cart_logo.png"
- "source": "https://s3bucket-domain/images/store-front-background.png",

## Developer environment

Amazon provides an online development environment to build Alexa Skills https://developer.amazon.com/alexa/console/ask

Amazon developer guide https://developer.amazon.com/en-US/alexa/alexa-skills-kit/start

Get started with development by using the online development environment or setup the Alexa SDK locally https://developer.amazon.com/en-US/docs/alexa/alexa-skills-kit-sdk-for-nodejs/set-up-the-sdk.html

### Intents

Intents represent the customer requests that your skill can handle. You define the intents for the unique functions that your skill provides, and can also take advantage of built-in intents for common actions such as stopping, canceling, and asking for help.

### Slots

Intents can optionally have arguments, or variables, called slots. You provide a name and a slot type for each slot in your interaction model. The slot type contains a list of representative values for the slot in order to improve recognition accuracy. You can use a built-in slot type for commonly used arguments such as dates and numbers, or define your own.

## Alexa Presentation Language

Alexa Presentation Language allows for the creation of visual elements that users can interact with. The solution uses a number of images to represent a default screen view as well as images for products. Images can be hosted anywhere e.g. an S3 bucket.

https://developer.amazon.com/en-US/docs/alexa/alexa-presentation-language/understand-apl.html

## Setup

**Step 1**

`npm install --save ask-sdk`

**Step 2**

Clone the repo into a local dev directory

**Step 3**

Run `npm install` to get npm packages

**Step 4**

Create an Alexa Developer Account as highlighted in the getting started Amazon documentation and update the constants highlighted in this document.

**Step 5**

Create an AWS Lambda function, zip and publish the project from http://aws.amazon.com/.

1.  If you have no Lambda functions yet, click **Get Started Now**.
2.  If you have one or more Lambda functions, click **Create function**.
3.  Select **Author from scratch**.
4.  For **Function name**, enter a name for your Lambda function.
5.  For **Runtime**, select **Node.js 10.x**.
6.  For **Permissions**, expand **Change default execution role**, and then select **Create a new role from AWS policy templates**.
7.  For **Role name**, enter a name for the role.
8.  From the **Policy templates** list, select **Simple Microservice permissions**.
9.  At the bottom of the page, click **Create function**.
10. Click **Function overview**, and then click **Add trigger**.
11. For **Trigger configuration**, select **Alexa Skills Kit**.
12. For **Skill ID verification**, select **Disable**.
13. Click **Add**.
14. In the middle of the page, click the **Code** tab.
15. On the right, click **Upload from**, and then select **.zip file**.

**Step 7**
Now that your skill code is in AWS Lambda, you can create and configure your skill in the ASK developer console.

**To create and configure an Alexa skill**

1.  Sign in to the [ASK developer console](https://developer.amazon.com/alexa/console/ask).
2.  Click **Create Skill**.
3.  Enter a name for your skill.
4.  For **Choose a model to add to your skill**, select **Custom**.
5.  For **Choose a method to host your skill's backend resources**, select **Provision your own**.
6.  At the top, click **Create skill**.
7.  For **Choose a template to add to your skill**, select **Start from Scratch**.
8.  At the top right, click **Choose**.
9.  On the left, click **Invocation**.
10. For **Skill Invocation Name**, enter `habitat grocers`, and then click **Save Model**.  
    To launch your skill, you will say, "Alexa, open Habitat Grocers."
