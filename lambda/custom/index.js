const Alexa = require('ask-sdk-core');
var request = require('request');
var http = require('http');
var https = require('https');

const s3bucket = 's3bucket-domain';
const hhcDemoHostUrl = 'sitecore-commerce-domain.com';
const awsLambdaHostUrl = 'lambda-host-domain.amazonaws.com';
const authKey = 'ENTER AUTH KEY';
const welcomeDocument = require('./APL/welcome.json');
const productListDocument = require('./APL/productlist.json');
const cartDocument = require('./APL/cart.json');
const successDocument = require('./APL/success.json');
const messagesDocument = require('./APL/messages.json');

const background_image_url =
  'https://s3bucket-domain/images/store-front-background.png';
const logo_image_url = 'https://s3bucket-domain/images/logo.png';

var productListing = [];
var cart = [];

function httpsGet(host, queryPath) {
  return new Promise((resolve, reject) => {
    var options = {
      host: host,
      port: 443,
      path: queryPath,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: authKey,
      },
    };

    const request = https.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        resolve(JSON.parse(returnData));
      });

      response.on('error', (error) => {
        reject(error);
      });
    });
    request.end();
  });
}

function httpPost(host, queryPath) {
  return new Promise((resolve, reject) => {
    var options = {
      host: host,
      port: 80,
      path: queryPath,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: authKey,
      },
    };

    const request = https.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        resolve(JSON.parse(returnData));
      });

      response.on('error', (error) => {
        return '';
      });
    });
    request.end();
  });
}

function httpGet(host, queryPath) {
  return new Promise((resolve, reject) => {
    var options = {
      host: host,
      port: 80,
      path: queryPath,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: authKey,
      },
    };

    console.log(options);

    const request = http.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        resolve(JSON.parse(returnData));
      });

      response.on('error', (error) => {
        reject(error);
      });
    });
    request.end();
  });
}

// Function: converts json to product object for APL
function convertProduct(item, index) {
  var newProduct = {
    listItemIdentifier: item.DisplayName,
    productId: item.ProductId,
    ordinalNumber: index,
    textContent: {
      primaryText: {
        type: 'PlainText',
        text: item.DisplayName,
      },
      secondaryText: {
        type: 'PlainText',
        text: `Price: ${item.AdjustedPriceWithCurrency}`,
      },
    },
    image: {
      contentDescription: null,
      smallSourceUrl: null,
      largeSourceUrl: null,
      sources: [
        {
          url: `${hhcDemoHostUrl}${item.SummaryImageUrl}`,
          size: 'small',
          widthPixels: 0,
          heightPixels: 0,
        },
        {
          url: `${hhcDemoHostUrl}${item.SummaryImageUrl}`,
          size: 'large',
          widthPixels: 0,
          heightPixels: 0,
        },
      ],
    },
    token: item.DisplayName,
  };
  productListing.push(newProduct);
}

// Function: get product stock info
async function getProductStockInfo(productId) {
  return await httpGet(
    hhcDemoHostUrl,
    `/api/cxa/alexacatalog/getproductstockinfo?productId=${productId}`
  );
}

// Function: add to cart
async function addToCart(productId, variantId, quantity) {
  console.log(
    `Attempting add to cart >> productId:${productId}, variantId:${variantId} quantity:${quantity}`
  );
  await httpPost(
    hhcDemoHostUrl,
    `/api/cxa/alexacart/addcartline?productId=${productId}&variantId=${variantId}&quantity=${quantity}`
  );
  console.log('Product added to cart');
}

// Proactive Event Handler (Notifications)
const ProactiveEventHandler = {
  canHandle(handlerInput) {
    console.log(handlerInput);
    return (
      handlerInput.requestEnvelope.request.type ===
      'AlexaSkillEvent.ProactiveSubscriptionChanged'
    );
  },
  async handle(handlerInput) {
    console.log(
      'AWS User ' + handlerInput.requestEnvelope.context.System.user.userId
    );
    console.log(
      'API Endpoint ' + handlerInput.requestEnvelope.context.System.apiEndpoint
    );
    console.log(
      'Permissions' +
        JSON.stringify(handlerInput.requestEnvelope.request.body.subscriptions)
    );
    await httpPost(
      hhcDemoHostUrl,
      `/api/cxa/alexanotifications/setamazonuseraccount?userAccountId=${handlerInput.requestEnvelope.context.System.user.userId}`
    );
  },
};

// Intent: Launch handler
const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest'
    );
  },
  handle(handlerInput) {
    const speakOutput =
      'Hello, welcome to Habitat Grocers! What can I help you with today?';
    const repromptText = 'What can I help you with today?';

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        token: '[SkillProvidedToken]',
        version: '1.0',
        document: welcomeDocument,
        datasources: {
          bodyTemplate6Data: {
            type: 'object',
            objectId: 'bt6Sample',
            backgroundImage: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: 'https://i2-prod.cambridge-news.co.uk/incoming/article14920227.ece/ALTERNATES/s1200b/1_GettyImages-838816102.jpg',
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'https://i2-prod.cambridge-news.co.uk/incoming/article14920227.ece/ALTERNATES/s1200b/1_GettyImages-838816102.jpg',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            image: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: 'http://images.media-allrecipes.com/userphotos/250x250/303241.jpg',
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'http://images.media-allrecipes.com/userphotos/250x250/303241.jpg',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            textContent: {
              primaryText: {
                type: 'PlainText',
                text: 'Welcome to The Daily Cheese',
              },
            },
            logoUrl:
              'https://d2o906d8ln7ui1.cloudfront.net/images/cheeseskillicon.png',
            hintText: 'Try, "Alexa, what are the specials today?"',
          },
        },
      })
      .getResponse();
  },
};

// Intent: Get product listing
const StoreOrderIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === 'StoreOrderIntent'
    );
  },
  async handle(handlerInput) {
    const foodItems =
      handlerInput.requestEnvelope.request.intent.slots.storeitem.value;
    const response = await httpGet(
      hhcDemoHostUrl,
      '/api/cxa/alexacatalog/getproducts?searchKeyword="' + foodItems + '"'
    );

    console.log(foodItems);
    console.log(response);

    // clear productListing
    productListing = [];
    var products = response.ChildProducts;
    products.forEach(convertProduct);
    console.log(productListing);

    const responseObject = await httpGet(
      hhcDemoHostUrl,
      '/api/cxa/alexaresponsephrases/getproducts?language=en'
    );
    const speakOutput = responseObject.SpeakOutput.replace(
      '${foodItems}',
      foodItems
    );
    const repromptText = `Would you like to add ${foodItems} to your basket?`;
    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(repromptText)
      .addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        token: '[SkillProvidedToken]',
        version: '1.0',
        document: productListDocument,
        datasources: {
          listTemplate2Metadata: {
            type: 'object',
            objectId: 'lt1Metadata',
            backgroundImage: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: background_image_url,
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'https://d2o906d8ln7ui1.cloudfront.net/images/LT2_Background.png',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            title: `Results for "${foodItems}"`,
            logoUrl: logo_image_url,
          },
          listTemplate2ListData: {
            type: 'list',
            listId: 'lt2Sample',
            totalNumberOfItems: 10,
            productname: 'Apples',
            hintText: 'Try, "Alexa, select number 1"',
            listPage: {
              listItems: productListing,
            },
          },
        },
      })
      .withShouldEndSession(false)
      .getResponse();
  },
};

// Intent: Add to Cart Intent
const AddToBasketIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === 'AddToBasketIntent'
    );
  },
  async handle(handlerInput) {
    const quantity =
      handlerInput.requestEnvelope.request.intent.slots.quantity.value;
    const foodItems =
      handlerInput.requestEnvelope.request.intent.slots.storeitem.value;

    var buyProduct;
    var productId;
    var hasFoundProduct = false;
    var searchProductName = foodItems;
    var cartTotal = 0;
    var variantId;
    var cartResponse;
    cart = [];

    // find the product matching the foodItem
    productListing.forEach(function findProduct(item, index) {
      console.log(
        'Searching for: ' +
          searchProductName.toLowerCase() +
          ' matching ' +
          item.listItemIdentifier.toLowerCase()
      );

      // find a match between product list and foodItem
      if (
        item.listItemIdentifier.toLowerCase() ===
        searchProductName.toLowerCase()
      ) {
        hasFoundProduct = true;
        buyProduct = item;
        productId = item.productId;

        console.log('Found Product? ' + hasFoundProduct);
        console.log('ProductId: ' + item.productId);
        console.log('Item: ' + buyProduct.listItemIdentifier);
      }
    });

    // Product was not found
    if (!hasFoundProduct) {
      productId = productListing[0].productId;
      console.log(`Calling getProductStockInfo(${productId})`);

      var product = await getProductStockInfo(productId);
      console.log(`variantId ${product.StockInformationList[0].VariantId}`);

      variantId = product.StockInformationList[0].VariantId;

      // add product to cart
      await addToCart(productId, variantId, quantity);

      console.log('Getting cart items');
      cartResponse = await httpGet(
        hhcDemoHostUrl,
        '/api/cxa/alexacart/getshoppingcart'
      );
      console.log(cartResponse);
    } else {
      console.log(`Calling getProductStockInfo(${productId})`);

      var product = await getProductStockInfo(productId);
      console.log(`variantId ${product.StockInformationList[0].VariantId}`);

      variantId = product.StockInformationList[0].VariantId;

      // add product to cart
      await addToCart(productId, variantId, quantity);

      // get cart items
      console.log('Getting cart items');
      cartResponse = await httpGet(
        hhcDemoHostUrl,
        '/api/cxa/alexacart/getshoppingcart'
      );
      console.log(cartResponse);
    }

    // convert json to object for APL
    cartResponse.Data.Lines.forEach(function convertCartProduct(item, index) {
      cartTotal += parseInt(item.LineTotal);
      var newProduct = {
        listItemIdentifier: item.DisplayName,
        productId: item.ProductId,
        ordinalNumber: index,
        textContent: {
          primaryText: {
            type: 'PlainText',
            text: item.DisplayName,
          },
          secondaryText: {
            type: 'PlainText',
            text: `Price: ${item.Quantity} x ${item.LinePrice}`,
          },
        },
        image: {
          contentDescription: null,
          smallSourceUrl: null,
          largeSourceUrl: null,
          sources: [
            {
              url: `${hhcDemoHostUrl}${item.Image}`,
              size: 'small',
              widthPixels: 0,
              heightPixels: 0,
            },
            {
              url: `${hhcDemoHostUrl}${item.Image}`,
              size: 'large',
              widthPixels: 0,
              heightPixels: 0,
            },
          ],
        },
        token: item.DisplayName,
      };
      cart.push(newProduct);
    });

    const speakOutput = `I have added ${quantity} pounds of ${foodItems} to your basket. Would you like to buy anything else or complete the purchase?`;
    const repromptText =
      'Would you like to buy anything else or complete the purchase?';
    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(repromptText)
      .addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        token: '[SkillProvidedToken]',
        version: '1.0',
        document: cartDocument,
        datasources: {
          listTemplate2Metadata: {
            type: 'object',
            objectId: 'lt1Metadata',
            backgroundImage: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: background_image_url,
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'https://d2o906d8ln7ui1.cloudfront.net/images/LT2_Background.png',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            title: `Cart Total = $${cartTotal}`,
            logoUrl: logo_image_url,
          },
          listTemplate2ListData: {
            type: 'list',
            listId: 'lt2Sample',
            totalNumberOfItems: 10,
            productname: 'Apples',
            hintText: 'Try, "I would like to complete my order"',
            listPage: {
              listItems: cart,
            },
          },
        },
      })
      .getResponse();
  },
};

// Intent: Complete Purchase
const CompletePurchaseIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      Alexa.getIntentName(handlerInput.requestEnvelope) ===
        'CompletePurchaseIntent'
    );
  },
  async handle(handlerInput) {
    // remove try catch, read the response model, look for error
    try {
      console.log('Attempting checkout');
      await httpPost(hhcDemoHostUrl, '/api/cxa/alexacheckout/completecheckout');
    } catch (err) {
      console.log('Failed checkout #1');
      console.log('Attempting checkout #2');
      await httpPost(hhcDemoHostUrl, '/api/cxa/alexacheckout/completecheckout');
    }

    // Empty cart after checkout
    cart = [];

    const responseObject = await httpGet(
      hhcDemoHostUrl,
      '/api/cxa/alexaresponsephrases/completecheckout?language=en'
    );
    const speakOutput = responseObject.SpeakOutput;
    return handlerInput.responseBuilder
      .speak(speakOutput)
      .addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        token: '[SkillProvidedToken]',
        version: '1.0',
        document: successDocument,
        datasources: {
          bodyTemplate7Data: {
            type: 'object',
            objectId: 'bt7Sample',
            title: 'Purchase Completed',
            backgroundImage: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: 'https://d2o906d8ln7ui1.cloudfront.net/images/BT7_Background.png',
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'https://d2o906d8ln7ui1.cloudfront.net/images/BT7_Background.png',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            image: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: background_image_url,
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'https://d2o906d8ln7ui1.cloudfront.net/images/MollyforBT7.png',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            logoUrl: logo_image_url,
            hintText: 'Try, "Alexa, search for blue cheese"',
          },
        },
      })
      .getResponse();
  },
};

// Intent: Get Specials
const GetSpecialsIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === 'GetSpecialsIntent'
    );
  },
  handle(handlerInput) {
    const speakOutput = `These are the specials tailored to you!`;
    return (
      handlerInput.responseBuilder
        .speak(speakOutput)
        //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
        .getResponse()
    );
  },
};

// Intent: Read messages
const ReadMessagesIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === 'ReadMessagesIntent'
    );
  },
  async handle(handlerInput) {
    const response = await httpsGet(awsLambdaHostUrl, '/Beta/getMessages');
    console.log(response);

    const message = response[response.length - 1].messageBody;
    console.log(message);

    const speakOutput = `These are your messages!`;
    return handlerInput.responseBuilder
      .speak(speakOutput)
      .addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        token: '[SkillProvidedToken]',
        version: '1.0',
        document: messagesDocument,
        datasources: {
          bodyTemplate1Data: {
            type: 'object',
            objectId: 'bt1Sample',
            backgroundImage: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: background_image_url,
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'https://d2o906d8ln7ui1.cloudfront.net/images/BT1_Background.png',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            title: 'Habitat Grocers: Messages',
            textContent: {
              primaryText: {
                type: 'PlainText',
                text: `${message}`,
              },
            },
            logoUrl: logo_image_url,
          },
        },
      })
      .getResponse();
  },
};

// Get basket items
const ReadBasketIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === 'ReadBasketIntent'
    );
  },
  async handle(handlerInput) {
    var cartTotal = 0;
    cart = [];

    // get cart items
    console.log('Getting cart items');
    const cartResponse = await httpGet(
      hhcDemoHostUrl,
      '/api/cxa/alexacart/getshoppingcart'
    );
    console.log(cartResponse);

    // convert json to object for APL
    cartResponse.Data.Lines.forEach(function convertCartProduct(item, index) {
      cartTotal += parseInt(item.LineTotal);
      var newProduct = {
        listItemIdentifier: item.DisplayName,
        productId: item.ProductId,
        ordinalNumber: index,
        textContent: {
          primaryText: {
            type: 'PlainText',
            text: item.DisplayName,
          },
          secondaryText: {
            type: 'PlainText',
            text: `Price: ${item.Quantity} x ${item.LinePrice}`,
          },
        },
        image: {
          contentDescription: null,
          smallSourceUrl: null,
          largeSourceUrl: null,
          sources: [
            {
              url: `${hhcDemoHostUrl}${item.Image}`,
              size: 'small',
              widthPixels: 0,
              heightPixels: 0,
            },
            {
              url: `${hhcDemoHostUrl}${item.Image}`,
              size: 'large',
              widthPixels: 0,
              heightPixels: 0,
            },
          ],
        },
        token: item.DisplayName,
      };
      cart.push(newProduct);
    });

    const speakOutput = `These are the items in your basket! Would you like to add anything else to your basket or complete the purchase?!`;
    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(
        'Would you like to add anything else to your basket or complete the purchase?!'
      )
      .addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        token: '[SkillProvidedToken]',
        version: '1.0',
        document: cartDocument,
        datasources: {
          listTemplate2Metadata: {
            type: 'object',
            objectId: 'lt1Metadata',
            backgroundImage: {
              contentDescription: null,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: background_image_url,
                  size: 'small',
                  widthPixels: 0,
                  heightPixels: 0,
                },
                {
                  url: 'https://d2o906d8ln7ui1.cloudfront.net/images/LT2_Background.png',
                  size: 'large',
                  widthPixels: 0,
                  heightPixels: 0,
                },
              ],
            },
            title: `Cart Total = $${cartTotal}`,
            logoUrl: logo_image_url,
          },
          listTemplate2ListData: {
            type: 'list',
            listId: 'lt2Sample',
            totalNumberOfItems: 10,
            productname: 'Apples',
            hintText: 'Try, "I would like to complete my order"',
            listPage: {
              listItems: cart,
            },
          },
        },
      })
      .getResponse();
  },
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent'
    );
  },
  handle(handlerInput) {
    const speakOutput = 'You can say hello to me! How can I help?';

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
      (Alexa.getIntentName(handlerInput.requestEnvelope) ===
        'AMAZON.CancelIntent' ||
        Alexa.getIntentName(handlerInput.requestEnvelope) ===
          'AMAZON.StopIntent')
    );
  },
  handle(handlerInput) {
    const speakOutput = 'Goodbye!';
    return handlerInput.responseBuilder.speak(speakOutput).getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) ===
      'SessionEndedRequest'
    );
  },
  handle(handlerInput) {
    // Any cleanup logic goes here.
    return handlerInput.responseBuilder.getResponse();
  },
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
    );
  },
  handle(handlerInput) {
    const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
    const speakOutput = `You just triggered ${intentName}`;

    return (
      handlerInput.responseBuilder
        .speak(speakOutput)
        //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
        .getResponse()
    );
  },
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`~~~~ Error handled: ${error.stack}`);
    const speakOutput = `Sorry, I had trouble doing what you asked. Please try again.`;

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
  .addRequestHandlers(
    LaunchRequestHandler,
    ProactiveEventHandler,
    StoreOrderIntentHandler,
    AddToBasketIntentHandler,
    CompletePurchaseIntentHandler,
    GetSpecialsIntentHandler,
    ReadBasketIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler,
    ReadMessagesIntentHandler,
    IntentReflectorHandler // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
